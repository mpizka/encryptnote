# encryptNote

simple AES256-CDC encrypted notetaking tool


# why

I made this as a little exercise with the `pycrypto` module.
Its a fun little program to play around with and will receive some
sort of passwd manager functionality later.

# who

Manfred PIZKA (mpizka@icloud.com)


# installation

requires: python3.7+, pycrypto, hashlib

* copy `en.py` to in one of your `$PATH` directories  
* maybe symlink to it (e.g. `ln -s en.py en`


# usage

```text
en new | list | add | show | del | chpwd | addln`
new
    create new note file in CWD (enotes.n)
    en always operates on enotes.n in CWD
list
    list notes in notefile
add TITLE CONTENT
    add note with title and content
show INDEX
    show note
del INDEX
    delete note
chpwd
    change passwd for notefile
addln INDEX LINE
    add line to content of note
```
